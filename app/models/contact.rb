class Contact < ApplicationRecord
  has_many :job_histories, :class_name => "ContactJobHistory"
  
  
  def self.search(category, search_key)
    
    if search_key.nil? or search_key.empty?
      return Contact.order(:name)
    else
      return Contact.where("#{category} LIKE ?", "%#{search_key}%").order(:name)
    end
    
  end
  
  
end
