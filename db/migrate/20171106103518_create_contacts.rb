class CreateContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
      t.string :company
      t.string :email
      t.string :city
      t.string :name
      t.string :country

      t.timestamps
    end
  end
end
