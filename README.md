#Search Contacts

This app is hosted at https://quiet-cove-83127.herokuapp.com/





##Description

There are two main components on the page, the search panel and the table listing 
the contacts' information. By default (or with an empty search term), all the contacts
in the database are displayed. The user can narrow down the list of contacts by 
selecting a category (name, company, city, or country) and search with a keyword. The
corresponding entries in the database that match the search keyword at the selected
category/column will be displayed.






##Main files

####Contacts

app/models/contact.rb

app/controllers/contacts_controller.rb

app/views/contacts/index.html.erb

app/views/contacts/_search_form.html.erb




####Contact Job Histories

app/models/contact_job_history.rb




####Rake task to load the data from json

lib/tasks/load_data.rake




####Database schema

db/schema.rb





####Contact

id      integer

name    string

email   string

company string

city    string

country string




####ContactJobHistory

id            integer

contact_id    reference

company       string





##Potential next steps

- Auto-complete search terms
- Add "job history" to search categories
- Search request through AJAX
