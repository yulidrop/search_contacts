require "rubygems"
require "yaml"
require "json"


namespace :load_data do
  
  desc "load sample data 1"
  task :sample1 => [:environment] do
    

    # load sample json data into memory
    file_path = "#{Rails.root.to_s}/exercise_data_June_2017.json"
    contacts_array = YAML.load(File.read(file_path))
    
    
    # contact_array: [ {
    #                     "job_history": [
    #                       "Sibelius"
    #                     ],
    #                     "company": "Mattis Corporation",
    #                     "email": "vehicula@et.com",
    #                     "city": "Westerlo",
    #                     "name": "David Harrington",
    #                     "country": "Spain"
    #                   },
    #                   .
    #                   .
    #                   .
    #                 ]
    
    
    
    # parse contact_array and store the data into database
    # for each contact, store everything except job_history into Contact table
    # for each job_hisotry of the contact, store it into ContactJobHistory table with reference to the corresponding Contact
    contacts_array.each do |contact_hash|
      
      new_contact = Contact.create({name: contact_hash["name"], company: contact_hash["company"], email: contact_hash["email"], city: contact_hash["city"], country: contact_hash["country"]})
      contact_hash["job_history"].each { |prev_comp| new_contact.job_histories.create({company: prev_comp})   }
      
    end
    

    
    
  end
  
end