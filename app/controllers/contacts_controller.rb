class ContactsController < ApplicationController
  def index
    
    @contacts = Contact.search(params[:category], params[:search_key])
    
  end
end
