class CreateContactJobHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :contact_job_histories do |t|
      t.references :contact, foreign_key: true
      t.string :company

      t.timestamps
    end
  end
end
